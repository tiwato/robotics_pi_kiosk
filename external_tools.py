import subprocess
import time
import json

import requests
import websocket
from urllib import parse
import threading
import typing
import sys


def max_volume():
    # amixer -c 0 set Master playback 100% unmute
    pass


class BrowerControl:
    ws: typing.Optional[websocket.WebSocket] = None
    ndi_active: bool = False

    def __init__(self):
        self.init_chromote()

    def chrome_rdp_send(self, json_msg):
        json_msg["id"] = 1
        self.ws.send(json.dumps(json_msg))
        res = self.ws.recv()
        #print("Chrome Rsp", res)
        return res

    def change_page(self, page: str):
        threading.Thread(target=self.change_page_aync, args=[page]).start()

    def get_page(self):
        try:
            a = self.chrome_rdp_send({"method": "Page.getFrameTree", "params": {}})
            return json.loads(a)['result']['frameTree']['frame']['url']
        except:
            return None


    def change_page_aync(self, page: str):
        if page.startswith("ndi://"):
            print("NDI Start")
            page = page.replace("ndi://", "")
            page = parse.quote(page)
            requests.get(f"http://localhost:8080/api/simple/player_stream?name={page}")
            requests.get(f"http://localhost:8080/api/simple/player_start")
            self.ndi_active = True

            return

        elif self.ndi_active:
            print("NDI Stop")
            self.ndi_active = False
            try:
                requests.get(f"http://localhost:8080/api/simple/player_stop")
            except requests.exceptions.ConnectionError:
                pass  # Work around until NDI enable code is added

        print("Update Page", page)
        # TODO Add firefox controls here

        if self.ws is None or not self.ws.connected:
            print("Connecting to Browser")

            if not self.init_chromote():
                print("Failed to connect to browser")
                return

        try:
            self.chrome_rdp_send({"method": "Page.navigate", "params": {"url": page}})
            time.sleep(3)
            # Send click to allow autoplay, since command line switches no longer work
            self.chrome_rdp_send({"method": "Input.dispatchMouseEvent",
                                  "params": {"type": "mousePressed", "x": 100, "y": 100, "button": "left",
                                             "clickCount": 1},
                                  "id": 1})
            self.chrome_rdp_send({"method": "Input.dispatchMouseEvent",
                                  "params": {"type": "mouseReleased", "x": 100, "y": 100, "button": "left",
                                             "clickCount": 1},
                                  "id": 1})

        except(ConnectionAbortedError, BrokenPipeError):
            print("Browser connection lost")
            self.ws = None

    def start_chrome(self, browser_name):
        print("STARTING BROWSER")

        if browser_name == "":
            return

        # if not self.init_chromote():
        #     return

        if sys.pathform == "win32":

            # browser = os.getenv("LOCALAPPDATA") + "/Google/Chrome/Application/chrome.exe"
            if browser_name == "chrome":
                browser_path = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"
            elif browser_name == "edge":
                browser_path = 'C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedge.exe'
            else:
                return
        else:
            if browser_name == "chrome":
                browser_path = "chromium-browser"
            elif browser_name == "firefox":
                browser_path = "firefox"
            else:
                return

        print(browser_path)
        args = [browser_path,
                "--incognito",
                # "--autoplay-policy=no-user-gesture-required",
                # "--no-user-gesture-required",
                "--remote-debugging-port=9222",
                "--no-first-run",
                "--user-data-dir=remote-debug-profile",  # Ensure it doesn't conflict with normal browser.
                # "--user-data-dir=.",
                # "--kiosk",
                # "--disable-web-security",
                ]
        subprocess.Popen(args)

    def init_chromote(self):
        try:
            res = requests.get('http://localhost:9222' + '/json')
        except requests.exceptions.ConnectionError:
            print("Failed to connect to browser")
            self.ws = None
            return False
        for tab in res.json():
            if tab['type'] == 'page':
                print('tab', tab['webSocketDebuggerUrl'])
                self.ws = websocket.WebSocket()
                self.ws.connect(tab['webSocketDebuggerUrl'])
                return True


if __name__ == '__main__':
    b = BrowerControl()
    b.start_chrome("edge")
    b.init_chromote()
