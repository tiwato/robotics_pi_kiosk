import socket
import serial
import os
from time import sleep

HOST = "0.0.0.0"
PORT = 5678


def main():
    if not os.path.exists('/dev/ttyUSB0'):
        return
    try:
        ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=0)
        print("Connected to Serial Port")

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen()
            while True:
                conn, addr = s.accept()
                print(f"Connected by {addr}")
                while True:
                    data = conn.recv(2048)
                    if not data:
                        break
                    # print(data)
                    ser.write(data)
                conn.close()
    except (serial.serialutil.SerialException, PermissionError) as e:
        print("Error in serial connection", e)
    except OSError as e:
        print("OSError", e)
    print("Sleeping 5 seconds")
    sleep(5)


if __name__ == '__main__':
    while 1:
        main()
