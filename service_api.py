import datetime
import re

import requests
from zeroconf import ServiceBrowser, Zeroconf, ServiceInfo, ServiceListener, InterfaceChoice, IPVersion
from socket import gethostname
from netifaces import interfaces, ifaddresses, AF_INET

import kioskmanager
import atexit
from external_tools import BrowerControl
import time
import urllib


# noinspection HttpUrlsUsage
class MyListener(ServiceListener):
    def __init__(self, kiosk_manager: kioskmanager.KioskManager):
        super().__init__()
        self.boot_time = datetime.datetime.now()
        self.got_pages = False
        self.kiosk_manager = kiosk_manager

    def remove_service(self, zc, type_, name):
        try:
            print("Service removed: %s," % (name,))
            name = name.replace(f".{type_}", "")
            if type_ == "_kiosk._tcp.local.":
                self.kiosk_manager.delete_kiosk(name)
            elif type_ == "_ndi._tcp.local.":
                ndi = self.kiosk_manager.ndi_sources
                del ndi[name]
                self.kiosk_manager.ndi_sources = ndi
            elif type_ == "_birddog._tcp.local.":
                self.kiosk_manager.delete_kiosk(name)
            else:
                raise Exception(f"Unknown type {type_}")
        except Exception as e:
            print("Exception raised", e)

    def add_service(self, zc, type_, name):
        self.add_update_service("added", zc, type_, name)

    def add_update_service(self, verb, zc, type_, name):
        try:
            info = zc.get_service_info(type_, name)
            if info is None:
                return
            print(
                f"Service {verb}: %s, service info: %s" % (name, (info.parsed_addresses(IPVersion.V4Only), info.port)),
                info.properties)

            # print(info,type_,name)
            name = name.replace(f".{type_}", "")
            if type_ == "_kiosk._tcp.local.":
                self.__ip_address_checker(name, info)
            elif type_ == "_ndi._tcp.local.":
                ndi = self.kiosk_manager.ndi_sources
                ndi[name] = f"ndi://{name}"
                self.kiosk_manager.ndi_sources = ndi
            elif type_ == "_birddog._tcp.local.":
                kiosk = self.kiosk_manager.get_by_name(name)

                full_page = ""
                setattr(kiosk, 'current_page', full_page)
                setattr(kiosk, 'group', 'None')
                setattr(kiosk, 'friendly_name', name.replace('._birddog._tcp.local.', ''))
                kiosk.api_type = "birddog"
                kiosk.ndi_support = True
                kiosk.browser_support = False
                kiosk.vnc_support = False
                kiosk.systemctl_support = False
                kiosk.ip_address = info.parsed_addresses(IPVersion.V4Only)[0] # FIXME: Is this better than .local?
                #kiosk.ip_address = f"{name}.local"
                kiosk.port = info.port
            else:
                raise Exception(f"******* Unknown type {type_}")

        except Exception as e:
            print("******* Exception raised", e)

    def update_service(self, zc, type_, name):
        self.add_update_service("update", zc, type_, name)

    def __ip_address_checker(self, name, info):
        kiosk = self.kiosk_manager.get_by_name(name)

        if datetime.datetime.now() - self.boot_time < datetime.timedelta(minutes=1) and not self.got_pages:
            # print("IN")
            try:
                # FIXME Corner cases if lots of kiosks boot at once?
                response = requests.get(f"http://{name}.local:{info.port}/get_pages", timeout=1)
                if response.status_code == 200 and len(response.json()) > 2:
                    new_data = response.json()
                    km = kioskmanager.KioskManager()
                    local_data = dict(km.pages)
                    local_data.update(new_data)
                    km.pages = local_data
                    self.got_pages = True

            except requests.exceptions.ConnectTimeout or requests.exceptions.ReadTimeout:
                pass
        full_page = (info.properties.get(b'page', b'None') +
                     info.properties.get(b'page2', b'') +
                     info.properties.get(b'page3', b'') +
                     info.properties.get(b'page4', b'')).decode()

        setattr(kiosk, 'current_page', full_page)
        setattr(kiosk, 'group', info.properties.get(b'group', b'None').decode())
        setattr(kiosk, 'friendly_name', info.properties.get(b'friendly', b'None').decode())
        kiosk.api_type = 'kiosk'
        kiosk.ndi_support = info.properties.get(b'ndi', False) == b'True'
        kiosk.browser_support = info.properties.get(b'browser', True) == b'True'
        kiosk.vnc_support = info.properties.get(b'vnc', False) == b'True'
        kiosk.systemctl_support = info.properties.get(b'systemctl', False) == b'True'
        # FIXME Duplicated code!
        kiosk.ip_address = info.parsed_addresses(IPVersion.V4Only)[0]  # FIXME: Is this better than .local?
        # kiosk.ip_address = f"{name}.local"
        kiosk.port = info.port


def ip4_addresses():
    ip_list = []
    for interface in interfaces():
        try:
            if AF_INET not in ifaddresses(interface):
                continue
            for link in ifaddresses(interface)[AF_INET]:
                if not link['addr'].startswith("127.0."):  # Loopback
                    ip_list.append(link['addr'])
        except ValueError:
            print("Invalid iface:")
    return ip_list

class ZeroConfWorker:
    zeroconf_single_if = None

    def update_zeroconf_service(self, port, properties):
        try:

            zc_type = "_kiosk._tcp.local."
            info = ServiceInfo(
                type_=zc_type,
                name=f'{gethostname()}.{zc_type}',
                properties=properties,
                parsed_addresses=ip4_addresses(),
                port=port,
                server=f"{gethostname()}.local",
            )
            if self.zeroconf_single_if is not None:
                print("Service changing:", f'{gethostname()}.{zc_type}', ip4_addresses(), properties)

                self.zeroconf_single_if.update_service(info)
            else:
                print("Service registering:", f'{gethostname()}.{zc_type}', ip4_addresses(), properties)
                self.zeroconf_single_if = Zeroconf()
                self.zeroconf_single_if.register_service(info, ttl=60, allow_name_change=False,
                                                         cooperating_responders=True)
                atexit.register(self.zeroconf_single_if.unregister_all_services)
                # print(zeroconf)
        except ValueError as e:
            print("-e- Value Error -- ", e)

    def __init__(self, port=80, ndi=True, vnc=True, browser=True, systemctl=True):
        self.port = port

        km = kioskmanager.KioskManager()
        self.browser = BrowerControl()
        self.last_time = time.time()

        if gethostname() in km.redis_server.keys():
            my_data = km.get_by_name(gethostname())
            page = my_data.current_page
            friendly = my_data.friendly_name
            group = my_data.group
        else:
            # Get current page on restart
            page = self.browser.get_page()
            if not page:
                page = "http://localhost"

            group = 'None'
            friendly = gethostname()

        self.properties = {"ndi"      : ndi,
                           "vnc"      : vnc,
                           "browser"  : browser,
                           "systemctl": systemctl,
                           "group"    : group,
                           "friendly" : friendly,
                           "updated"  : datetime.datetime.now()
                           }
        self.unpack_page(self.properties, page)

        self.update_zeroconf_service(self.port, self.properties)

    def localize_page(self, page):
        if page == "http://localhost" and self.port != 80:
            page = f"http://localhost:{self.port}"
        if re.search(r"/event/\w+/display", page):
            name = urllib.parse.quote(self.properties["friendly"])
            page += "&name=" + name

        return page

    def delocalize_page(self, page):
        if page == f"http://localhost:{self.port}/" and self.port != 80:
            page = "http://localhost"

        name = urllib.parse.quote(self.properties["friendly"])
        page = page.replace("&name=" + name, "")

        return page

    def compare_page(self, properties, page2):
        page1 = properties['page'] + properties['page2'] + properties['page3'] + properties['page4']
        return page1.strip('/') == page2.strip('/')

    def unpack_page(self, properties, page):
        properties['page'] = page[0:200]
        properties['page2'] = page[200:400]
        properties['page3'] = page[400:600]
        properties['page4'] = page[600:800]

    def zeroconf_worker(self):
        self.last_time = time.time()
        while 1:
            self.zc_redis_monitor_loop()
            # Check for local browser changes
            if time.time() - self.last_time > 10:
                self.last_time = time.time()

                data = self.browser.get_page()
                if data is None:
                    continue
                data = self.delocalize_page(data)

                if self.compare_page(self.properties, data):
                    continue
                if self.properties['page'].startswith("ndi"):
                    continue

                self.unpack_page(self.properties, data)

                self.properties["updated"] = datetime.datetime.now()
                print("Out of band page change detected")
                self.update_zeroconf_service(self.port, self.properties)

    def zc_redis_monitor_loop(self):
        """
        Check for updates in redis queues for page, name or group updates
        :return:
        """
        self.properties["updated"] = datetime.datetime.now()

        # print("updated", properties["updated"])
        data_tuple = kioskmanager.redis_server.blpop(["page_queue", "name_queue", "group_queue"], timeout=10)
        if data_tuple is None:
            # Timeout
            return
        msg_type, data = data_tuple
        print("Process", msg_type, data)
        if msg_type == "page_queue":
            if self.compare_page(self.properties, data):
                return
            self.unpack_page(self.properties, data)

            data = self.localize_page(data)
            self.browser.change_page(data)

        elif msg_type == "name_queue":
            if self.properties['friendly'] == data:
                return
            self.properties['friendly'] = data
        elif msg_type == "group_queue":
            if self.properties['group'] == data:
                return
            self.properties['group'] = data
        self.update_zeroconf_service(self.port, self.properties)


def init_zc_browser():
    km = kioskmanager.KioskManager()

    # Clean up all old kiosks. They will refresh once the listenr fires below
    km.ndi_sources = {}
    for kiosk in km.all_kiosks():
        if kiosk.name != gethostname():  # .local isn't saved in redis
            km.delete_kiosk(kiosk.name)

    listener = MyListener(km)
    listen_types = ["_kiosk._tcp.local.",  # Other Kiosks
                    "_ndi._tcp.local.",  # NDI Sources
                    "_birddog._tcp.local."]  # Birddog decoders
    ServiceBrowser(Zeroconf(interfaces=InterfaceChoice.All), listen_types, listener)
