#!/usr/bin/python3

from flask import Flask, render_template
import requests
import json
import os

app = Flask(__name__)

app.config['TEMPLATES_AUTO_RELOAD'] = True


@app.route("/")
def announcer():
    return render_template("announcer.html", apikey=data["apikey"], eventCode=data["eventCode"],
                           scoreIP=data["scoreIP"])


def getIP():
    ip = input(f"What is the IP address of the scoring system (default={data['scoreIP']})?")
    if ip:
        return ip
    return data["scoreIP"]


def getEvent():
    event_rsp = requests.get(f"http://{data['scoreIP']}/api/v1/events/")
    if event_rsp.status_code != 200:
        print("Can't find scoring system")
        exit(-1)
    codes = event_rsp.json()['eventCodes']

    if len(codes) == 1:
        return codes[0]
    message = 'Pick your event:\n'

    for index, code in enumerate(codes):
        message += f"{code}\n"

    message += "Selection:"
    choice = ''
    while choice.lower() not in codes:
        choice = input(message)
    return choice


if os.path.exists("announcer.cfg"):
    data = json.load(open("announcer.cfg"))
else:
    data = {
        "apikey"   : "eXAcpyqXpMkSACsUmIKQNpwAOOVamNxz",
        "eventCode": "zzz",
        "scoreIP"  : "score.local"
    }

data["scoreIP"] = getIP()
data["eventCode"] = getEvent()

print(data)
a = requests.get(f"http://{data['scoreIP']}/api/v1/keycheck/", headers={"Authorization": data['apikey']})
if a.status_code == 404:
    b = requests.post(f"http://{data['scoreIP']}/api/v1/keyrequest/?name=Announcer%20Report")
    if b.status_code != 200:
        print(b.reason)
        print("Error requesting key")
    else:

        print(f"New Key {b.json()['key']}")
        data['apikey'] = b.json()['key']

print(a.json())
json.dump(data, open("announcer.cfg", 'w'))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
