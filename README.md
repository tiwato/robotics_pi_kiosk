# Overview
This is a tool to help with quickly setting up remote displays for events using Raspberry Pis or the like.  It can allow centralized control of any number of displays, controlling what webpages they are displaying. 

# Installation Instructions
1) Install Python3.7+
2) `python -m venv env`
3) Linux - `source env/bin/activate`
4) Windows  - `env\Scripts\activate.bat` 
5) `pip install -r requirements.txt`
6) `python app.py`


# TODO
* [x] Finish initial clean of POC version
* [x] write setup docs
* [x] Update gitlab repo
* [X] Add better smartness for kiosk mode  
* [X] Write auto-start script
* [X] Build Pi SD card image
* [ ] Allow Config save/restore/reset
* [ ] Add remote WiFi Control
* [x] Add in proxy and zerotier (VPN) support to allow mixed networks (e.g. ORTOP_SCORE + school wifi)
* [X] Allow control of other tools (e.g. VLC)