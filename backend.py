from service_api import ZeroConfWorker, init_zc_browser

from media_server import media_server
import threading

init_zc_browser()
threading.Thread(target=media_server, daemon=True).start()
ZeroConfWorker(80).zeroconf_worker()

