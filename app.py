#!/usr/bin/python3
import logging
import subprocess
from flask import Flask, render_template, request, redirect, url_for, jsonify, make_response, send_file, Response
import requests
from requests_futures.sessions import FuturesSession
import socket
import argparse
from socket import gethostname
import os
from werkzeug.utils import secure_filename
import glob
from collections import defaultdict
import sys
import redisdl

import service_api
import conf
from media_server import send_command
from kioskmanager import KioskManager, multi_post
import qrcode
from qrcode.image import svg

km = KioskManager()
host_kiosk = km.get_by_name(gethostname())
if not host_kiosk:
    host_kiosk = km.add_kiosk(gethostname(), True)
    host_kiosk.current_page = "http://localhost"

group_list = ["Fields", "Audience", "Pits", "None"]  # TODO Add support for defining groups

session = FuturesSession()

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = 'media_dir/'
app.config['MAX_CONTENT_LENGTH'] = 500 * 1024 * 1024

# noinspection SpellCheckingInspection
logging.basicConfig(stream=sys.stdout, level=logging.WARNING,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


@app.route("/qrcode", methods=["GET"])
def get_qrcode():
    data = request.args.get("data", "")
    label = request.args.get("label", "")
    global_scale = int(request.args.get("scale", 1))

    a = qrcode.make(data, image_factory=svg.SvgFragmentImage)

    out = '<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink">'
    out += f'<g transform="scale({global_scale})" >'
    out += f'<g transform="scale(5)" fill="#000000"><path d="M22.7,0H1.3C0.6,0,0,0.6,0,1.3v25.3C0,27.4,0.6,28,1.3,28h21.3c0.7,0,1.3-0.6,1.3-1.3V1.3C24,0.6,23.4,0,22.7,0 z M23,22c0,0.6-0.5,1-1,1H2c-0.6,0-1-0.5-1-1V2c0-0.6,0.5-1,1-1h20c0.6,0,1,0.5,1,1V22z"/></g>'
    out += f'<g transform="scale({330 / a.pixel_size * .95})" >'

    out += a.to_string().decode()

    out += '</g><text fill="#FFFFFF" font-family="Arial, Helvetica, sans-serif" font-size="15px" text-anchor="middle" x="60" y="133" >'
    out += label
    out += '</text></g></svg>'

    return Response(out, mimetype='image/svg+xml')


@app.route("/tablet", methods=["GET"])
def tablet_codes():
    return render_template('tablet_qr.html')  #


@app.context_processor
def build_nav_bar():
    nav = [
        {"text": "Who Am I", "url": url_for('id_page')},
        {"text": "Config Kiosk", "url": url_for('update_kiosk')},
        {"text": 'Config Pages', "url": url_for('config_pages')},
        {"text": 'Matrix Control', "url": url_for('matrix_selector')},
        {"text": 'NDI Control',
             "sublinks": [
                 {"text": x.friendly_name, "url": url_for('iframe')+f"?url={x.ndi_url}&hint={x.ndi_password}"} for x in km.all_kiosks() if x.ndi_support == "true"
             ],
         },
        {"text"    : 'VNC Control',
         "sublinks": [
             {"text": x.friendly_name, "url": url_for('iframe') + f"?url={x.vnc_url}&hint={x.vnc_password}"} for x in km.all_kiosks() if
             x.vnc_support == "true"
         ],
         },
         #"url": url_for('ndi_iframe')},
        #{"text": 'Media Control', "url": url_for('media')},
        #{"text": 'Upload video', "url": url_for('upload_video')},
        {"text": 'Tablet QRCodes', "url": url_for('tablet_codes')},
    # {
    #     "text": "More",
    #     "sublinks": [
    #         {"text": "Stack Overflow", "url": "https://stackoverflow.com"},
    #         {"text": "Google", "url": "https://google.com/"},
    #     ],
    # },
    ]

    return dict(navbar=nav)


@app.route('/save')
def save_db():
    json_text = redisdl.dumps()
    return json_text


@app.route('/clear_redis')
def clear_redis():
    km.redis_server.flushall()
    return "OK"


@app.route('/pages')
def config_pages():
    return render_template('config_pages.html', all_pages=km.pages)  #


@app.route('/restart_service')
def restart_service():
    print("Restarting Service")
    os.system('/usr/bin/sudo /usr/bin/systemctl restart robotics_pi_kiosk_backend.service')
    return "OK"


@app.route('/shutdown_stick')
def shutdown_stick():
    print("Shutting down!")
    os.system('/usr/bin/sudo /usr/sbin/shutdown now')
    return "OK"


# noinspection HttpUrlsUsage
@app.route("/ajax_add_event", methods=["POST", "GET"])
def ajax_add_event():
    if request.method == 'POST':
        event = request.form['event']
        ip = request.form['ip']
        prefix = request.form['prefix']

        print("Pages", km.pages)
        p = km.pages

        p[prefix + "Audience"] = f"http://{ip}/event/{event}/display/?type=audience&bindToField=all&scoringBarLocation=bottom&allianceOrientation=standard&liveScores=true&mute=false&muteRandomizationResults=false&fieldStyleTimer=false&overlay=false&overlayColor=%2300FF00&allianceSelectionStyle=classic&awardsStyle=overlay&previewStyle=overlay&randomStyle=overlay&dualDivisionRankingStyle=sideBySide&rankingsFontSize=larger&rankingsShowQR=false&showMeetRankings=false&rankingsAllTeams=true"
        p[prefix + "Field 1"] = f"http://{ip}/event/{event}/display/?type=field&bindToField=1&scoringBarLocation=bottom&allianceOrientation=standard&liveScores=false&mute=true&muteRandomizationResults=false&fieldStyleTimer=true&overlay=false&overlayColor=%2300FF00&allianceSelectionStyle=classic&awardsStyle=overlay&previewStyle=overlay&randomStyle=overlay&dualDivisionRankingStyle=sideBySide&rankingsFontSize=larger&rankingsShowQR=false&showMeetRankings=false&rankingsAllTeams=true"
        p[prefix + "Field 2"] = f"http://{ip}/event/{event}/display/?type=field&bindToField=2&scoringBarLocation=bottom&allianceOrientation=standard&liveScores=false&mute=true&muteRandomizationResults=false&fieldStyleTimer=true&overlay=false&overlayColor=%2300FF00&allianceSelectionStyle=classic&awardsStyle=overlay&previewStyle=overlay&randomStyle=overlay&dualDivisionRankingStyle=sideBySide&rankingsFontSize=larger&rankingsShowQR=false&showMeetRankings=false&rankingsAllTeams=true"
        p[prefix + "Pit"] = f"http://{ip}/event/{event}/display/?type=pit&bindToField=all&scoringBarLocation=bottom&allianceOrientation=standard&liveScores=false&mute=true&muteRandomizationResults=false&fieldStyleTimer=false&overlay=false&overlayColor=%2300FF00&allianceSelectionStyle=classic&awardsStyle=overlay&previewStyle=overlay&randomStyle=overlay&dualDivisionRankingStyle=sideBySide&rankingsFontSize=larger&rankingsShowQR=true&showMeetRankings=false&rankingsAllTeams=true"
        p[prefix + "Remote Pit"] = f"https://ftc-scoring.firstinspires.org/event/{event}/display/pit?type=pit&bindToField=all&scoringBarLocation=bottom&allianceOrientation=standard&liveScores=true&mute=false&muteRandomizationResults=false&fieldStyleTimer=false&overlay=false&overlayColor=%2300FF00&allianceSelectionStyle=classic&awardsStyle=overlay&dualDivisionRankingStyle=sideBySide&rankingsFontSize=larger&rankingsShowQR=true&showMeetRankings=false&rankingsAllTeams=true"
        p[prefix + "Check-In status"] = f"http://{ip}/event/{event}/status/proj/2/"

        km.pages = p
        print(km.pages)
    return "OK"


@app.route("/ajax_add_update", methods=["POST", "GET"])
def ajax_add_update():
    if request.method == 'POST':

        name = request.form['name']
        url = request.form['url']
        record_id = request.form['record_id']
        local_pages = km.pages
        x = 0


        # print(new_data)
        if name == '':
            msg = 'Please Input name'
        elif url == '':
            msg = 'Please Input URL'
        elif record_id == '-1':
            local_pages[name] = url
            msg = 'New record created successfully'
        else:
            del local_pages[record_id]
            local_pages[name] = url
            msg = 'Record successfully Updated'
        km.pages = local_pages
        print(km.pages)
        return jsonify(msg)


@app.route("/get_pages")
def get_pages():
    return jsonify(km.pages)

@app.route("/set_pages", methods=["POST"])
def set_pages():
    if request.method == 'POST':
        print("set_pages", dict(request.form))
        km.silent_page(dict(request.form))
        return "OK"

@app.route("/reset_pages", methods=["POST"])
def reset_pages():
    print("Got Reset")
    km.pages = {"Example": "https://example.com", "Info": "http://localhost"}
    return "OK"

@app.route("/ajax_delete", methods=["POST", "GET"])
def ajax_delete():
    if request.method == 'POST':
        local_pages = km.pages  # Work via local copy, since it won't delete otherwise
        try:
            del local_pages[request.form['record_id']]
            msg = 'Record deleted successfully'
        except KeyError:
            msg = "Error when deleting"

        km.pages = local_pages
        return jsonify(msg)


@app.route('/api/media/<string:command>', methods=['POST'])
def api_media(command):
    data = request.get_json()
    if data.get("file"):
        return send_command([command, os.path.join(app.config['UPLOAD_FOLDER'], data.get("file"))])
    else:
        return send_command([command, None])


# noinspection HttpUrlsUsage
@app.route('/api/share_media/', methods=['POST'])
def api_share():
    # TODO Make this better.
    data = request.json
    kiosk = km.get_by_name(data["kiosk"])
    file = data["file"]
    file_data = open(os.path.join(app.config['UPLOAD_FOLDER'], file), "rb")
    print("Start Upload")
    a = requests.post(f"http://{kiosk.api_url}/upload-video", files={"file": (file, file_data)})
    print("Done")
    return str(a)


@app.route('/api/media/<string:command>_all', methods=['POST'])
def api_media_all(command):
    data = request.get_json()

    kiosks = km.get_by_names(data["kiosks"])
    kiosk_data = data.copy()
    del kiosk_data["kiosks"]

    multi_post(kiosks, f"api/media/{command}", kiosk_data, timeout=10)

    return f"OK - {command}-all"


@app.route("/api/media/files")
def get_media():
    return jsonify(get_file_list())


@app.route('/set_data', methods=['GET', 'POST'])
def set_data():
    print("set_data", dict(request.form))
    host_kiosk.all = dict(request.form)
    return "OK"


@app.route('/get_page')
def get_page(): return host_kiosk.current_page


@app.route('/get_name')
def get_name(): return host_kiosk.friendly_name


@app.route('/get_group')
def get_group(): return host_kiosk.group


@app.route('/get_data')
def get_data():
    out = {}
    for x in ["group", "friendly_name", "current_page"]:
        out[x] = getattr(host_kiosk, x)
    out["media_files"] = get_file_list()
    return out


@app.route('/')
def id_page():

    version = "Unknown"
    for cmd in ["git describe --exact-match --tags", "git rev-parse --short HEAD"]:
        out = subprocess.run(cmd.split(), capture_output=True)
        if out.returncode == 0:
            version = out.stdout.decode().strip()
            break

    return render_template('kiosk_id.html',
                           name=gethostname(),
                           ip_address=service_api.ip4_addresses(),
                           ssid=get_ssid(),
                           friendly_name=host_kiosk.friendly_name,
                           group=host_kiosk.group,
                           version=version)


@app.route('/iframe', methods=["GET"])
def iframe():

    return render_template('ndi_iframe.html', url=request.args.get("url"), password=request.args.get("hint") )


def get_undef_pages():
    undef_pages = {}
    for kiosk_data in km.get_kiosk_data():
        page = kiosk_data['current_page']
        if page is None or page == "":  # Dead browser, Birddog display, boot delay, etc
            continue

        if page in km.pages.values() or page.startswith("ndi"):  # Good pages or NDI screens
            continue

        undef_pages[page] = page
    return undef_pages


@app.route('/matrix')
def matrix_selector():
    return render_template('matrix.html',
                           all_pages=km.pages,
                           undef_pages=get_undef_pages(),
                           ndi_displays=km.ndi_sources,
                           displays=km.get_kiosk_data(),
                           groups=km.get_group_data())


@app.route('/matrix_update', methods=["POST", "GET"])
def matrix_ajax():
    if request.method == 'POST':
        kiosk_name = request.form['display']
        page = request.form['page']

        if kiosk_name in [x['name'] for x in km.get_kiosk_data()]:
            kiosk = km.get_by_name(kiosk_name)
            api_url = kiosk.api_url

            if page in km.pages:
                kiosk.current_page = km.pages[page]
                send_update(f"http://{api_url}/set_data", {"current_page": kiosk.current_page})
            elif page in km.ndi_sources:
                kiosk.current_page = km.ndi_sources[page]

                if kiosk.api_type == "kiosk":
                    send_update(f"http://{api_url}/set_data", {"current_page": kiosk.current_page})
                elif kiosk.api_type == "birddog":
                    send_update(f"http://{api_url}/connectTo", {"sourceName": kiosk.current_page.replace("ndi://", "")})

            else:
                print("ERROR - Sending Matrix update to", api_url, page)
        else:
            for kiosk in km.all_kiosks():
                if kiosk.group == kiosk_name:
                    api_url = kiosk.api_url

                    if page in km.pages:
                        kiosk.current_page = km.pages[page]
                        send_update(f"http://{api_url}/set_data", {"current_page": kiosk.current_page})
                    if page in km.ndi_sources:
                        kiosk.current_page = km.ndi_sources[page]
                        send_update(f"http://{api_url}/set_data", {"current_page": kiosk.current_page})

            print("GROUP LOOP")
    msg = {"all_pages": km.pages, "undef_pages": get_undef_pages(), "ndi_displays": km.ndi_sources, "displays": km.get_kiosk_data(),
           "groups"   : km.get_group_data()}
    return jsonify(msg)


@app.route('/config', methods=['GET', 'POST'])
def update_kiosk():
    if request.method == "POST":
        out = defaultdict(dict)
        for (k, v) in request.form.items():
            server_name, data_key = k.split('|')
            kiosk = km.get_by_name(server_name)
            if data_key in ["group", "friendly_name"]:
                kiosk.__setattr__(data_key, v)
            if kiosk != host_kiosk:
                api_url = kiosk.api_url
                out[api_url][data_key] = v

        for api_url, data in out.items():
            print(f"Sending update to {api_url} {data}")
            send_update(f"http://{api_url}/set_data", data)

        return redirect(url_for('update_kiosk'))
    else:
        return render_template('config_displays.html', names=km.get_kiosk_data(), groups=group_list, urls=km.pages)


@app.route("/media")
def media():
    return render_template("media_control.html", media_list=host_kiosk.media_files, kiosks=km.all_kiosks())


@app.route("/upload-video", methods=["GET", "POST"])
def upload_video():
    if request.method == "POST":
        file = request.files["file"]
        filename = secure_filename(file.filename)
        if not os.path.exists(app.config['UPLOAD_FOLDER']):
            os.makedirs(app.config['UPLOAD_FOLDER'])
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        print("File uploaded")

        return make_response(jsonify({"message": "File uploaded"}), 200)

    return render_template("upload_video.html")


def send_update(url, data=None):
    print(url, data)
    try:
        session.post(url, data, timeout=3)
    except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError, socket.timeout):
        print("Update error")
        pass


def get_file_list():
    files = glob.glob(os.path.join(app.config['UPLOAD_FOLDER'], "*"))
    return [os.path.basename(file) for file in files]


def get_ssid():
    try:
        # noinspection SpellCheckingInspection
        result = subprocess.run(["/usr/sbin/iwgetid"], capture_output=True)
        if result.returncode > 0:
            return "Not Wifi"
        return result.stdout.decode().split('"')[1]
    except FileNotFoundError:
        return "Unknown"



if __name__ == "app" and app.env == "development":
    conf.start_wrapper(5000)

if __name__ == '__main__':
    # files = {'file': open('ftc.mkv','rb')}
    # a = requests.post("http://kiosk3.localdomain/upload-video",files=files)
    # print(a)
    # exit()
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    # logging.disable(logging.INFO)
    service_api.port = 5000

    parser = argparse.ArgumentParser(description="Args for kiosk")
    browsers = ["chrome", "edge", "firefox"]
    parser.add_argument("browser", type=str, default="edge", choices=browsers)
    args = parser.parse_args()

    conf.start_wrapper(5000)

    app.run(host="0.0.0.0", )
