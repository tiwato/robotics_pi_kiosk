from service_api import ZeroConfWorker, init_zc_browser

from media_server import media_server
import threading


# noinspection PyUnusedLocal
def on_starting(server):
    #start_wrapper(80)
    pass


def start_wrapper(port):
    init_zc_browser()
    threading.Thread(target=media_server, daemon=True).start()
    threading.Thread(target=ZeroConfWorker(port, ndi=False, vnc=False, systemctl=False).zeroconf_worker, daemon=True).start()


