from __future__ import annotations  # For py3.8
import json
import redis
import fakeredis
import os
from multiprocessing import Pool
import requests
from natsort import natsorted
from socket import gethostname

if os.name == 'nt':
    redis_server = fakeredis.FakeStrictRedis(charset="utf-8", decode_responses=True)
else:
    redis_server: redis = redis.Redis(host='localhost', port=6379, db=0, charset="utf-8", decode_responses=True)


class KioskItem:
    def __init__(self, name):
        self.name = name
        self.local = name == gethostname()
        self.redis_server = redis_server

    def __get(self, item):
        return self.redis_server.hget(self.name, item)

    def __set(self, item, value):
        self.redis_server.hset(self.name, item, json.dumps(value))

    @property
    def browser_support(self):
        return self.__get("browser_support")

    @browser_support.setter
    def browser_support(self, value):
        self.__set("browser_support", value)

    @property
    def ndi_support(self):
        return self.__get("ndi_support")

    @ndi_support.setter
    def ndi_support(self, value):
        self.__set("ndi_support", value)

    @property
    def api_type(self):
        return self.__get("api_type")

    @api_type.setter
    def api_type(self, value):
        self.redis_server.hset(self.name, "api_type", value)

    @property
    def vnc_support(self):
        return self.__get("vnc_support")

    @vnc_support.setter
    def vnc_support(self, value):
        self.__set("vnc_support", value)

    @property
    def systemctl_support(self):
        return self.__get("systemctl_support")

    @systemctl_support.setter
    def systemctl_support(self, value):
        self.__set("systemctl_support", value)

    @property
    def current_page(self):
        return self.__get("current_page")

    @current_page.setter
    def current_page(self, value):
        old_page = self.__get("current_page")
        self.redis_server.hset(self.name, "current_page", value)
        if self.local and old_page != value:
            self.redis_server.rpush("page_queue", value)

    @property
    def group(self):
        return self.__get("group")

    @group.setter
    def group(self, value):
        old_value = self.__get("group")
        self.redis_server.hset(self.name, "group", value)
        if self.local and old_value != value:
            self.redis_server.rpush("group_queue", value)

    @property
    def friendly_name(self):
        return str(self.__get("friendly_name"))

    @friendly_name.setter
    def friendly_name(self, value):
        old_value = self.__get("friendly_name")
        self.redis_server.hset(self.name, "friendly_name", value)
        if self.local and old_value != value:
            self.redis_server.rpush("name_queue", value)

    @property
    def main_url(self):
        if self.port != 80 and self.api_type != 'birddog':  # TODO swap to kiosk subclasses to handle types
            return f"{self.ip_address}:{self.port}"
        else:
            return self.ip_address

    @property
    def api_url(self):
        return f"{self.ip_address}:{self.port}"

    @property
    def ip_address(self):
        return self.__get("ip_address")

    @ip_address.setter
    def ip_address(self, value):
        self.redis_server.hset(self.name, "ip_address", value)

    @property
    def port(self):
        return self.__get("port")

    @port.setter
    def port(self, value):
        self.redis_server.hset(self.name, "port", value)

    @property
    def media_files(self):
        try:
            return json.loads(self.__get('media_files'))
        except TypeError:
            return []

    @media_files.setter
    def media_files(self, value: list):
        self.__set("media_files", value)

    @property
    def all(self):

        return {"current_page": self.current_page,
                "name": self.name,
                "ip_address": self.ip_address,
                "main_url": self.main_url,
                "api_url": self.api_url,
                "ndi_address": self.ndi_url,
                "vnc_address": self.vnc_url,
                "friendly_name": self.friendly_name,
                "media_files": self.media_files,
                "group": self.group,
                "ndi_support": self.ndi_support,
                "browser_support": self.browser_support,
                "vnc_support": self.vnc_support,
                "systemctl_support": self.systemctl_support}

    @property
    def vnc_url(self):
        return f"http://{self.ip_address}:6080/vnc.html"

    @property
    def vnc_password(self):
        return "a"

    @property
    def ndi_url(self):
        if self.api_type == "birddog":
            return f"http://{self.ip_address}"
        else:
            return f"http://{self.ip_address}:8080/streamer"

    @property
    def ndi_password(self):
        if self.api_type == "birddog":
            return "birddog"
        else:
            return "admin"

    @all.setter
    def all(self, value):
        for key in value:
            if key in ["current_page", "name", "ip_address", "friendly_name", "group", "media_files", "ndi_support",
                       "browser_support", "vnc_support", "systemctl_support"]:
                if value[key] is not None:
                    self.__setattr__(key, value[key])

    def __lt__(self, other):
        return self.friendly_name < other.friendly_name


class KioskManager:
    def __init__(self):
        self.redis_server = redis_server

    @property
    def pages(self):
        try:
            return json.loads(self.redis_server.get('pages'))
        except TypeError:
            return {"Example": "https://example.com", "Info": "http://localhost"}

    def silent_page(self, value: dict):
        """
        Set the pages value without triggering reflection storm
        :param value:
        :return:
        """
        self.redis_server.set("pages", json.dumps(value))

    @pages.setter
    def pages(self, value: dict):
        if self.redis_server.get("pages") == json.dumps(value):
            return
        self.redis_server.set("pages", json.dumps(value))
        multi_post(self.all_other_kiosks(), "set_pages", {}, data=value)


    @property
    def ndi_sources(self):
        try:
            return json.loads(self.redis_server.get('ndi'))
        except TypeError:
            return {}

    @ndi_sources.setter
    def ndi_sources(self, value: list):
        self.redis_server.set("ndi", json.dumps(value))

    @staticmethod
    def get_by_name(name):
        return KioskItem(name)

    @staticmethod
    def get_by_names(names: list[str]):
        return [KioskItem(name) for name in names]

    def all_kiosks(self):
        misc_keys = {"pages", "ndi", "page_queue", "name_queue", "group_queue"}
        return [KioskItem(name) for name in natsorted(self.redis_server.keys()) if name not in misc_keys]

    def all_other_kiosks(self):
        return [x for x in self.all_kiosks() if x.name != gethostname()]

    def delete_kiosk(self, name):
        if name in self.redis_server.keys() and name != gethostname():
            self.redis_server.delete(name)

    @staticmethod
    def add_kiosk(name, local=False):
        kiosk = KioskItem(name, local)
        kiosk.friendly_name = name
        kiosk.group = "None"
        kiosk.current_page = "http://localhost"
        return kiosk

    def get_kiosk_data(self):
        all_data = []

        for kiosk in self.all_kiosks():
            all_data.append(kiosk.all)
        return all_data

    def get_group_data(self):
        all_data = {}
        for kiosk in self.all_kiosks():
            if kiosk.group == "None":
                continue
            if kiosk.group not in all_data:
                all_data[kiosk.group] = {"name": kiosk.group,
                                         "friendly_name": kiosk.group,
                                         "current_pages": {kiosk.current_page},
                                         "members": [kiosk.name]}
            else:
                all_data[kiosk.group]["members"].append(kiosk.name)
                all_data[kiosk.group]["current_pages"].add(kiosk.current_page)

        out = []
        for x in all_data.values():
            x["current_pages"] = list(x["current_pages"])
            if len(x["current_pages"]) == 1:
                x["current_page"] = x["current_pages"][0]
                del x["current_pages"]
            out.append(x)

        return out


def post_wrapper(args):
    (url, params, kwargs) = args
    try:
        requests.post(url, json=params, **kwargs,timeout=.0001)
    except requests.exceptions.RequestException:
        return "FakeOK"


def multi_post(kiosks: list[KioskItem], path="api_play", *args, **kwargs):
    for kiosk in kiosks:
        if kiosk.ip_address is None:
            continue
        post_wrapper((f"http://{kiosk.ip_address}/{path}", *args, kwargs))

