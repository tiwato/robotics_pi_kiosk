import sys
from time import sleep

import os

if os.name == 'nt':
    os.add_dll_directory(r"c:\Program Files\VideoLAN\VLC")

import vlc
import socket
import json

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_socket():
    return socket.socket(socket.AF_INET, socket.SOCK_STREAM)


def send_command(*msg):
    msg = json.dumps(*msg).encode()
    logger.debug(f"Command: {msg}")
    host = '127.0.0.1'  # The server's hostname or IP address
    port = 65432  # The port used by the server
    with get_socket() as s:
        s.connect((host, port))
        s.sendall(msg)
        data = s.recv(1024)
    return data


def media_server():
    m: vlc.MediaPlayer
    host = '127.0.0.1'  # Standard loopback interface address (localhost)
    port = 65432  # Port to listen on (non-privileged ports are > 1023)
    with get_socket() as s:
        s.bind((host, port))
        s.listen()
        while True:
            try:
                conn, addr = s.accept()
                with conn:
                    # print('Connected by', addr)
                    while True:
                        data = conn.recv(4096)

                        # print("data", data)
                        if not data:
                            break

                        data = json.loads(data.decode())
                        logger.debug(f"Data: {data}")
                        # if not data:
                        #     command = ""
                        rsp = "Unknown"
                        command = data[0]
                        logger.info(f"MediaCmd: {command}")
                        # arg1 = data[1]
                        if command == "load" and len(data) == 2:
                            if m:
                                m.stop()
                            m = vlc.MediaPlayer(data[1])
                            m.set_fullscreen(True)

                            m.play()
                            sleep(.1)
                            m.set_pause(True)
                            m.set_time(0)
                            rsp = "OK - load"
                        if m:
                            if command == "play":
                                m.play()
                                rsp = "OK"
                            elif command == "stop":
                                m.stop()
                                rsp = "OK"
                            elif command == "pause":
                                m.pause()
                                rsp = "OK"
                            elif command == "set_pos":
                                m.set_time(int(data[1]))
                                rsp = "OK"
                            elif command == "getspu":
                                rsp = f"{m.video_get_spu_description()}  {m.video_get_spu()}"

                            elif command == "setspu":
                                m.video_set_spu(int(data[1]))

                        logger.debug(f"Reply {rsp}")
                        conn.sendall(rsp.encode())
            except Exception as e:
                logger.error("Exception thrown in media server", e)
                pass


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("need s or c as argument")
    elif sys.argv[1] == "s":

        media_server()
    elif sys.argv[1] == "c":
        cmd_data = json.dumps(sys.argv[2:]).encode()
        print(cmd_data)
        cmd_data = sys.argv[2:]
        print(cmd_data)
        cmd_data = send_command(cmd_data)

        print('Received', repr(cmd_data))
    else:
        print(f"no can do: {sys.argv[1]}")
